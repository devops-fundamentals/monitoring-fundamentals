# Monitoring with Prometheus and Grafana

---

## Lab Infrastructure

 - Monitoring Server: Used to deploy Prometheus, Alertmanager and Grafana as docker containers
 - Application Server: Ubuntu server to be used as target for monitoring

## Install Node Exporter in the target server

### Download Node Exporter

 - Connect to the target server and move to the opt folder:
```
cd /opt
```

 - Download the node exporter binaries:
```
sudo wget "https://github.com/prometheus/node_exporter/releases/download/v0.17.0/node_exporter-0.17.0.linux-amd64.tar.gz"
```

 - Extract the downloaded file:
```
sudo tar -xvzf "./node_exporter-0.17.0.linux-amd64.tar.gz"
```

 - Move the binaries folder to the "/opt" folder:
```
sudo cp "./node_exporter-0.17.0.linux-amd64/node_exporter" "/opt/node_exporter"
```

 - Remove the obsolete files:
```
sudo rm -rf "node_exporter-0.17.0.linux-amd64" "node_exporter-0.17.0.linux-amd64.tar.gz"
```

### Configure Node Exporter

 - Create a service file using the command:
```
sudo nano /etc/systemd/system/node_exporter.service
```

 - With the content below:
```
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target
 
[Service]
User=docker
ExecStart=/opt/node_exporter
 
[Install]
WantedBy=default.target
```

 - Note:
```
- Under [Service] set the user who is going to run the service
 
- If you want to run node_exporter in a different port, add the flag "--web.listen-address" to the ExecStart, for example:
 
   ExecStart="/opt/node_exporter/node_exporter" "--web.listen-address=:9110"
```

 - Configure node exporter to run as a service:
```
sudo systemctl daemon-reload
```
```
sudo systemctl start node_exporter
```
```
sudo systemctl enable node_exporter
```

### Check that Node Exporter is working as expected 

 - To check that “node_export” is working browse to:
```
http://<target-server-ip>:9100/metrics
```

## Configure and deploy Prometheus

 - Access to the monitoring server and create a new folder:
```
mkdir ~/monitoring
```

 - Create a new folder for the prometheus configuration:
```
mkdir ~/monitoring/prometheus
```

 - Create the "targets.yml" file with the content below:
```
nano ~/monitoring/prometheus/targets.yml
```
```
- targets: ['<application-server-ip>:9100']
  labels:
    name: 'Application'
```

 - Create the prometheus configuration file with the content below:
```
nano ~/monitoring/prometheus/prometheus.yml
```
```
# Global Configuration
global:
  scrape_interval:     15s
  evaluation_interval: 15s 
  scrape_timeout: 10s

# Scrape Configuration
scrape_configs:
  - job_name: 'Application Monitoring'
    file_sd_configs:
      - files:
         - targets.yml
```

 - Run prometheus as docker container using the created configuration files:
```
docker run -d --name prometheus -p 9090:9090 -v ~/monitoring/prometheus:/etc/prometheus --restart always prom/prometheus:v2.7.1
```

- Browse to prometheus and confirm that is up and running:
```
http://<monitoring-server-ip>:9090
```

- Browse to the targets page and confirm that the application server is being monitored:
```
http://<monitoring-server-ip>:9090/targets
```

## Configure and deploy Grafana

 - Run grafana as docker container using the command below:
```
docker run -d --name grafana -p 3000:3000 -v grafana:/var/lib/grafana --restart always grafana/grafana:5.4.3
```

- Browse to grafana and confirm that is up and running:
```
http://<monitoring-server-ip>:3000
```

- Login using the following credentials:
```
Username: admin
Password: admin
```

- Click skip, and then add a new data source:
```
Type: Prometheus
Name: Prometheus
Url: http://<monitoring-server-ip>:9090
(keep default values in everything else)
```

- From the '+' menu select "import dashboard" and paste the grafana dashboard id below:
```
1860
```

## Configure and deploy Alertmanager (Optional)

 - Note: you will need your own smpt server to send notifications (I will use my gmail account)

  - Configure prometheus to communicate with alertmanager by update the "prometheus.yml" file:
```
nano ~/monitoring/prometheus/prometheus.yml
``` 
```
# Global Config
global:
  scrape_interval:     15s
  evaluation_interval: 15s 
  scrape_timeout: 10s

# Alertmanager configuration
alerting:
  alertmanagers:
  - scheme: http
    static_configs:
    - targets: ['<monitoring-server-ip>:9093']
rule_files:
  - 'alerts.yml'

# Scrape Configuration
scrape_configs:
  - job_name: 'servers'
    file_sd_configs:
      - files:
         - targets.yml
```

 - Let's create two alerts for prometheus using the "alerts.yml" file:
```
nano ~/monitoring/prometheus/alerts.yml
``` 
```
groups:
  - name: critical-alerts
    rules:
    - alert: 'Instance Down'
      expr: up == 0
      for: 1m
      labels:
        severity: critical
      annotations:
        summary: "[ {{ $labels.name }} ] is down"
        description: "[ {{ $labels.name }} ] has been down for more than 1 minute"
  - name: usage-alerts
    rules:
    - alert: 'RAM Usage'
      expr: 100 - ((node_memory_MemAvailable_bytes * 100) / node_memory_MemTotal_bytes) > 15
      for: 3m
      labels:
        severity: warning
      annotations:
        summary: "Server {{ $labels.instance }} reach the RAM memory limit"
        description: "Server {{ $labels.instance }} has been using more than 85% of his memory RAM in the last 3 minutes"
    - alert: 'Disk Usage'
      expr: max(((node_filesystem_size_bytes{fstype=~"ext4|vfat"} - node_filesystem_free_bytes{fstype=~"ext4|vfat"}) / node_filesystem_size_bytes{fstype=~"ext4|vfat"}) * 100) by (name) > 20
      for: 5m
      labels:
        severity: warning
      annotations:
        summary: "Server {{ $labels.instance }} reach the disk usage limit"
        description: "Server {{ $labels.instance }} has been using more than 85% of his disk space in the last 5 minutes"
    - alert: 'CPU Usage'
      expr: (1 - avg(irate(node_cpu_seconds_total{mode="idle"}[10m])) by (name)) * 100 > 5
      for: 5m
      labels:
        severity: warning
      annotations:
        summary: "Server {{ $labels.instance }} reach the cpu usage limit"
        description: "Server {{ $labels.instance }} has been using more than 85% of his cpu in the last 5 minutes"
``` 

 - Create a new folder for the alertmanager configuration:
```
mkdir ~/monitoring/alertmanager
```

 - Create the "targets.yml" file with the content below:
```
nano ~/monitoring/alertmanager/alertmanager.yml
```
```
global:
  smtp_smarthost: 'smtp.gmail.com:587'
  smtp_from: 'your-gmail-account@gmail.com'
  smtp_auth_username: 'your-gmail-account@gmail.com'
  smtp_auth_identity: 'your-gmail-account@gmail.com'
  smtp_auth_password: 'PASSWORD'
  smtp_require_tls: true

route:
  group_by: ['severity']
  # When a new group of alerts is created by an incoming alert, wait at least 'group_wait' to send the initial notification
  group_wait: 30s
  # When the first notification was sent, wait 'group_interval' to send a batch of new alerts
  group_interval: 1m
  # If an alert has successfully been sent, wait 'repeat_interval' to resend them
  repeat_interval: 6h
  # Default receivers
  receiver: 'email'

# Inhibition rules allow to mute a set of alerts given that another alert is firing.
inhibit_rules:
- source_match:
    severity: 'critical'
  target_match:
    severity: 'warning'

# Receivers details (email, slack, integrations configs, etc)
receivers:
  - name: 'email'
    email_configs:
    - to: 'your-gmail-account@gmail.com'
```

 - Run alertmanager as a docker container using the command below:
```
docker run -d --name alertmanager -p 9093:9093 -v ~/monitoring/alertmanager:/etc/alertmanager --restart always prom/alertmanager:v0.16.1
```

 - Restart the prometheus container to apply the changes:
```
docker restart prometheus
```

 - Browse to the prometheus "alerts" page and confirm the configuration:
```
http://<monitoring-server-ip>:9090/alerts
```

 - Browse to the alertmanager page and confirm is up and running:
```
http://<monitoring-server-ip>:9093
```

## Cleanup

 - Remove the running containers:
```
docker rm -f $(docker ps -a -q)
``` 
